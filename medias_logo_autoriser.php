<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'appel pour le pipeline
 *
 * @pipeline autoriser
 */
function medias_logo_autoriser() {
}
function autoriser_document_transformer_logo_dist($faire, $type, $id, $qui, $opt) {
	// cas particulier (hack nouvel objet)
	if (intval($id) < 0 and $id == -$qui['id_auteur']) {
		return true;
	}

	return autoriser('modifier', $type, $id, $qui, $opt);
}
