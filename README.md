Pour Spip4.0 et 4.1 : un bouton pour passer une image liée a un objet en logo de cet objet 

Offre la même fonctionnalité que https://git.spip.net/spip-contrib-extensions/medias_logos qui le faisait pour Spip3 
(la gestion des logos a été techniquement refondue avec SPIP4, d'où le besoin de ce plugin)

En attendant que ça devienne une fonctionnalité générique à la version Spip4.2