<?php
if (!defined("_ECRIRE_INC_VERSION")) return;
 
 
// il y a un pipeline medias_editer_document_actions dans medias/medias_pipeline
/*Ce pipeline permet aux plugins d'ajouter de boutons d'action supplémentaires
  sur les formulaires d'édition de documents */
 
 
 
 
/// --------
//doc https://programmer.spip.net/Contenu-d-un-fichier-action
//et pris en modele medias/actions/dissocier_document.php
/*  fournit les arguments de la fonction  sous la forme `$id_objet-$objet-$document-suppr-safe`*/
function action_document_transformer_logo($arg = null){
	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}
	
	// attention au cas ou id_objet est negatif !
	if (strncmp($arg, '-', 1) == 0) {
		$arg = explode('-', substr($arg, 1));
		[$id_objet, $objet, $document] = $arg;
		$id_objet = -$id_objet;
	} else {
		$arg = explode('-', $arg);
		[$id_objet, $objet, $document] = $arg;
	}
//	$suppr = $check = false;
	if (count($arg) > 3 and $arg[3] == 'transfologo') {
		$transfologo = true;
	}
	if (count($arg) > 4 and $arg[4] == 'safe') {
		$check = true;
	}
	if ($id_objet = intval($id_objet) or autoriser('document_transformer_logo', $objet, $id_objet)
		)
			{
//document2logo
$id_document=$document;
	/*	-		selectionner tous les doc lié a l objet + retirer les logoon */	
	if ($r = sql_select(
'id_document','spip_documents_liens',
 'id_objet='.$id_objet.' and objet="'.$objet.'"')) {
		while ($ligne = sql_fetch($r)) {
sql_updateq('spip_documents', array('mode' => 'image'), 'id_document='. $ligne['id_document'].' and mode="logoon"');				
spip_log("plugins documents2logo mode image pour iddoc $id_document $objet $id_objet",'documents2logo');
	}
	}

//-	pour id_doc dont on parle, pour l objet en cours, passer en logoon+invalider le cache
//repris de function action_changer_mode_document_post, plus generique, mais qui limite au 3 modes 'vignette', 'image', 'document' en spip4 et 4.1
include_spip('action/editer_document');
document_modifier($id_document, ['mode' => 'logoon']);

// Invalider les caches
include_spip('inc/invalideur');
suivre_invalideur("id='document/$id_document'");			
				
			}
				else{
spip_log("Interdit de transforemer en logo ",'documents2logo'._LOG_ERREUR);
				}
}